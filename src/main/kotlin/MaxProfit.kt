package ciolli.rob.maxProfit

fun maxProfit(c: Array<Int>): Int {
    return c.foldIndexed(0){ idx, acc1, i1 ->
        c.copyOfRange(idx + 1, c.size).fold(acc1) { acc2, i2 ->
            val diff = i2 - i1
            if (diff > acc2) diff else acc2
        }
    }
}