package ciolli.rob.maxProfit.tests

import ciolli.rob.maxProfit.maxProfit
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class ArrayTests {

    @Test
    fun bestStockPriceTest() {

        val testCases = arrayOf(
            Pair(0, arrayOf(10, 9, 8, 7, 6, 5, 4, 3, 2, 1)),
            Pair(6, arrayOf(10, 5, 8, 3, 9)),
            Pair(4, arrayOf(1, 2, 3, 4, 5))
        )

        testCases.forEach {
            Assertions.assertEquals(
                it.first, maxProfit(it.second)
            )
        }
    }
}
