# Latitude max profit test
#### rob ciolli

---

## Prerequisites

This code requires Java 1.8 to be installed with JAVA_HOME set

## Execute code

This solution uses a single junit test to execute the solution.

execute:

```./gradlew clean test```

in the root directory to run

Extra test cases can be added to MaxProfitTests.kt file

